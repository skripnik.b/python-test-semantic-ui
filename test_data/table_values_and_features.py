from selene.api import s, ss, by
from selene.browser import driver
from selenium.common import exceptions

list_of_test_parameters = (
    ["Error", "Jimmy", 2, "Cannot pull data"],  # Return OK
    ["Positive / Negative", "No Name Specified", 2, "Approved"],  # Return ERROR
    ["Positive / Negative", "No Name Specified", 3, "None"],  # Return OK
    ["Positive / Negative", "Jill", 2, "Approved"],  # Return ERROR
    ["Positive / Negative", "Jill", 3, "None"],  # Return OK
)

url_table = 'https://semantic-ui.com/collections/table.html'
url_dropdown = 'https://semantic-ui.com/modules/dropdown.html'
url_checkbox = 'https://semantic-ui.com/modules/checkbox.html'


def comparing_tables_values(*kwargs):
    """
    :arg[0] tb_title, like "Error"
    :arg[1] tb_user_name, like "Jamie"
    :arg[2] tb_column, like "Cannot pull data"
    :arg[3] checked_value, like "None"
    :return True/False
    """

    print()
    print("kwargs[2] === ", kwargs[2])
    name_error_table = f"//div[@class='example']/h4[text()='{kwargs[0]}']" \
                       f"/../table/tbody/tr/td[text()='{kwargs[1]}']" \
                       f"/../td[{kwargs[2]}]"

    name_error_table_value = s(by.xpath(name_error_table)).text

    if kwargs[3] == name_error_table_value:
        return True
    return False


def is_sign_exist(**kwargs):
    """
    :param kwargs: tb_title, tb_name
    :return: True/False
    """

    xpath_for_row = f"//div[@class='example']/h4[text()='{kwargs['tb_title']}']" \
                    f"/../table/tbody/tr/td[text()='{kwargs['tb_name']}']" \
                    f"/../td"

    table_column_count = len(ss(by.xpath(xpath_for_row)))

    for i in range(table_column_count):
        is_elem_exist_2 = f"//div[@class='example']/h4[text()='{kwargs['tb_title']}']" \
                          f"/../table/tbody/tr/td[text()='{kwargs['tb_name']}']" \
                          f"/../td[{i+1}]/i"
        try:
            if driver().find_element_by_xpath(is_elem_exist_2):
                xpath_head_table = f"//div[@class='example']/h4[text()='Warning']/../" \
                                   f"table[@class='ui celled table']/thead/tr/th[{i+1}]"
                print()
                print(f"Sign present for '{kwargs['tb_name']}' in table_column # {s(by.xpath(xpath_head_table)).text}")
                return True
        except exceptions.NoSuchElementException as e:
            # print('There is no sign', e)
            pass
