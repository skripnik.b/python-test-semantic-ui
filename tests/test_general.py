import pytest
from selene.api import be
from selene.browser import open_url
from test_data.table_values_and_features import *

# 1. Go to the https://semantic-ui.com/collections/table.html
# 2. In the "Positive / Negative" table check, that "Jimmy" has status "Cannot pull data".
# 3. In the "Positive / Negative" table check, that two names "No Name Specified" & "Jill"
# has _Status_ "Approved" & _Notes_ "None".
@pytest.mark.usefixtures("custom_browser")
@pytest.mark.parametrize("url", [url_table])
@pytest.mark.parametrize("tb_title, tb_user_name, tb_column, checked_value", list_of_test_parameters)
def test_table_part_1(url, tb_title, tb_user_name, tb_column, checked_value):

    open_url(url)
    assert comparing_tables_values(tb_title, tb_user_name, tb_column, checked_value)


# 4. In the "Warning" table, check that "Jimmy" & "Jamie" have "!" sign in their own table_row
@pytest.mark.usefixtures("custom_browser")
@pytest.mark.parametrize("url", [url_table])
def test_table_part_2(url):
    """
    :param url:
    :return: True/False as result of asserts
    """

    open_url(url)

    assert is_sign_exist(tb_title="Warning", tb_name="Jimmy"), "There is no '!' - sign"
    assert is_sign_exist(tb_title="Warning", tb_name="Jamie"), "There is no '!' - sign"


# Test #2
# 1. Go to the https://semantic-ui.com/modules/dropdown.html
# 2. For "Selection" in the first drop-down list choose "Female" in the second - "Male", in the third - "Christian".
# 3. Check that chosen values were accepted
@pytest.mark.usefixtures("custom_browser")
@pytest.mark.parametrize("url", [url_dropdown])
def test_dropdown(url):
    """
    :param url:
    :return: True/False as result of asserts
    """
    open_url(url)

    # 1-st select

    first_gender_selection = "//h4[@class='ui header' and text()= 'Selection']/../div[@class='ui selection dropdown']"
    s(by.xpath(first_gender_selection)).click()
    first_select = "//h4[@class='ui header' and text()= 'Selection']/../" \
                   "div[@class='ui selection dropdown active visible']/" \
                   "div[@class='menu transition visible']/div[text()='Female']"
    s(by.xpath(first_select)).click()

    # 2-nd select

    second_gender_selection = "//div[@class='ui active tab']/div[@class='another dropdown example']/" \
                              "div[@class='ui dropdown selection']"
    s(by.xpath(second_gender_selection)).click()
    second_select = "//div[@class='ui active tab']/div[@class='another dropdown example']/" \
                    "div[@class='ui dropdown selection active visible']/div/div[text()='Male']"
    s(by.xpath(second_select)).click()

    # 3-rd select

    friend_selection = "//div[@class='ui active tab']/div[@class='another dropdown example']/" \
                       "div[@class='ui fluid selection dropdown']"
    s(by.xpath(friend_selection)).click()
    third_select = "//div[@class='ui active tab']/div[@class='another dropdown example']/" \
                   "div[@class='ui fluid selection dropdown upward active visible']/div/div[@data-value='christian']"
    s(by.xpath(third_select)).click()

    # ACCEPTANCE

    first_set_text = "//h4[@class='ui header' and text()= 'Selection']/../div[@class='ui selection dropdown']/" \
                     "div[@class='text']"
    second_set_text = "//div[@class='ui active tab']/div[@class='another dropdown example']/div/" \
                      "div[contains(text(), 'Male')]"
    third_set_text = "//div[@class='ui active tab']/div[@class='another dropdown example']/div/" \
                     "div[@class='text']/img/.."

    first_field_set_text = s(first_set_text).text
    second_field_set_text = s(second_set_text).text
    friend_set_text = s(third_set_text).text

    assert first_field_set_text == 'Female'
    assert second_field_set_text == 'Male'
    assert 'Christian' in friend_set_text


# Test №3
# 1. Go to the https://semantic-ui.com/modules/checkbox.html
# 2. Change value for checkboxes, radio buttons, sliders и toggle.
# 3. Check that chosen values were accepted
@pytest.mark.usefixtures("custom_browser")
@pytest.mark.parametrize("url", [url_checkbox])
def test_checkbox(url):
    """
    :param: url
    :return: True/False as result of asserts
    """

    open_url(url)

    # 1 - checkbox. Activate 'Make my profile visible'.

    checkbox_xpath = "//div[@class ='ui checkbox']/label[text()='Make my profile visible']"
    s(by.xpath(checkbox_xpath)).click()
    selected_checkbox_xpath = "//div[@class ='ui checkbox checked']/label[text()='Make my profile visible']"

    # 2 - radio buttons. Activate 'Once a day'

    radio_xpath = "//div[@class='inline fields']/div/div[@class ='ui radio checkbox']/label[text()='Once a day']"
    s(by.xpath(radio_xpath)).click()
    selected_radio_xpath = "//div[@class='inline fields']/div/div[@class ='ui radio checkbox checked']/label[text()='Once a day']"

    # 3 - sliders. Activate 'Accept terms and conditions'

    slider_xpath = "//div[@class='ui slider checkbox']/label[text()='Accept terms and conditions']"
    s(by.xpath(slider_xpath)).click()
    selected_slider_xpath = "//div[@class='ui slider checkbox checked']/label[text()='Accept terms and conditions']"

    # 4 - toggle. Activate 'Subscribe to weekly newsletter'

    toggle_xpath = "//div[@class ='ui toggle checkbox']/label[text()='Subscribe to weekly newsletter']"
    s(by.xpath(toggle_xpath)).click()
    selected_toggle_xpath = "//div[@class ='ui toggle checkbox checked']/label[text()='Subscribe to weekly newsletter']"

    # ACCEPTANCE

    assert s(by.xpath(selected_checkbox_xpath)).should(be.visible)
    assert s(by.xpath(selected_radio_xpath)).should(be.visible)
    assert s(by.xpath(selected_slider_xpath)).should(be.visible)
    assert s(by.xpath(selected_toggle_xpath)).should(be.visible)
