import os
import pytest
from selenium import webdriver
from selene import browser, config
from selene.browser import driver, set_driver


@pytest.fixture()
def custom_browser():
    # Setup
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")
    custom_browser.driver = webdriver.Chrome(options=chrome_options)
    custom_browser.driver.set_window_size(1920, 1080)
    config.reports_folder = os.path.join(os.curdir)
    set_driver(custom_browser.driver)
    yield driver()
    # Teardown
    browser.quit()
